#!/usr/bin/env python3
import pydot
import numpy as np
import itertools as it


class pydotFlow():
    def __init__(self,nfile,lfile,cfile):
        self.components = list()
        self.components.extend(('nodes','links','classes'))
        self.raw_data = {}
        self.raw_data['nodes'] = pydotFlow.import_file(nfile)
        self.raw_data['links'] = pydotFlow.import_file(lfile)
        self.raw_data['classes'] = pydotFlow.import_file(cfile)
        self.default_comp =  {}                                                                 
        self.default_comp['nodes'] =  { 'name' : 'empty' }                                      
        self.default_comp['links'] =  {'tag' : '',  'traits' : 'straight', 'width' : 1 , 'color' : 'black' }
        self.default_comp['classes'] =  { 'shape' : 'box' }                                     
        self.nodes =  { }                                     

    @staticmethod
    def import_file(filename):
        import_data = np.genfromtxt(filename, dtype="str", delimiter = "\t")
        return import_data

    @staticmethod                      
    def product_entries(entries):
        data_list = list()
        for column in range(0,len(entries)):
            data_list.append(entries[column].split(','))
        return list(it.product(data_list[0],data_list[1])) # trick as I will have only two columns in all my files so just consider first columns but could handle more by modifying this.

        
    @staticmethod
    def demultiplex_entries(array_entries):
        demultiplexed_entries = list()
        for entry in range(0,len(array_entries)):
            demultiplexed_entries.extend(pydotFlow.product_entries(array_entries[entry]))
        return demultiplexed_entries

    @staticmethod                                                                          
    def dictionnary_maker(settings:list):                                                  
        settings_split = [i.split(':',1) for i in settings] # this generate a list of lists
        dictionnary =  {}                                                                  
        for i in range(0, len(settings_split)):                                                                          
            dictionnary[settings_split[i][0]] = settings_split[i][1]                       
        return dictionnary                                                                 

    
    @staticmethod
    def parser_metadata(in_data:str , default:dict):
        in_split = in_data[in_data.find("[")+1:in_data.find("]")]
        if len(in_split) < len(in_data)-1:
            parameters = in_split.split(';')
            parameters_parsed = pydotFlow.dictionnary_maker(parameters)
            
            return parameters_parsed
        else:
            return default
   

    @staticmethod
    def parser_data(in_data:list):
        
        
        variable = list()
        variable.append(in_data[0].split("[")[0])
        variable.append(in_data[1])
        
                             
        return variable

    def class_form_parser(self,comp:str):
        self.flow_chart_data[comp] = {'metadata' : list(), 'data' : list()}
        for c in self.data[comp]:
            self.flow_chart_data[comp]['metadata'] +=  [pydotFlow.parser_metadata(c[0], self.default_comp[comp])]
            self.flow_chart_data[comp]['data']  +=  [pydotFlow.parser_data(c)]


    def get_class_settings(self, node:list):
        position_class = self.flow_chart_data['nodes']['data'].index(node)
        return self.flow_chart_data['classes']['metadata'][position_class], position_class

    def check_link_metadata (self, link_metadata:dict):
        print(link_metadata)
        for settings in self.default_comp['links'].keys():
            if link_metadata.get(settings) == None:
               link_metadata[settings] = self.default_comp['links'][settings]
        
        
        return link_metadata


        
    def build_flow_chart(self):
        self.flow_chart = pydot.Dot(graph_type='digraph') # initialize graph
        # magic starts here as we generate variable by using names instide variable
        self.data =  {}
        self.flow_chart_data = {}  # declared here but used somewhere else
        for component in self.components:
            self.data[component] = pydotFlow.demultiplex_entries(self.raw_data[component])
            pydotFlow.class_form_parser(self,component)
        # create nodes links and use classes when needed.
        for nodeIt in self.flow_chart_data['nodes']['data']:
            node_class_data, node_index = self.get_class_settings(nodeIt)
            exec( f"self.nodes[nodeIt[0]]" + "= self.make_node(self.flow_chart_data['nodes']['metadata'][node_index]['name'],node_class_data['shape'])")
        for linkIt in self.flow_chart_data['links']['data']:
            index_link = self.flow_chart_data['links']['data'].index(linkIt)
            metadata = self.check_link_metadata(self.flow_chart_data['links']['metadata'][index_link])
            exec(f"link_{index_link}" + "= self.make_link(self.nodes[linkIt[0]], self.nodes[linkIt[1]], label = metadata['tag'], width = metadata['width'], style = metadata['traits'], color = metadata['color'])")
        # for classIt in self.data['classes']:
        #     pass                       
            

        
    
    def make_node(self,name,shape):                                             
        cur_node = pydot.Node(name)                                        
        cur_node.set_shape(shape)                                          
        self.flow_chart.add_node(cur_node)                                       
        return cur_node                                                    
                                                                           
                                                                               
    def make_link(self,a_node, b_node, label = None, width = 1, style='dashed', color = 'black'):
        cur_edge = pydot.Edge(a_node,b_node)                               
        cur_edge.set_penwidth(width)                                       
        cur_edge.set_style(style)
        cur_edge.set_color(color)
        if label is not None: cur_edge.set_label(label)                    
        self.flow_chart.add_edge(cur_edge)                                       
        return cur_edge                                                    


    def plot_and_save(self,orientation,saveName):
        if (orientation == 'horizonal'):
            self.flow_chart.set_rankdir('LR')
        elif (orientation == 'vertical'):
            self.flow_chart.set_rankdir('UD')
        else:
            print('Error, please choose an orientation between horizontal and vertical')
            pass
        save= f"{saveName}.svg"
        self.flow_chart.write_svg(save, prog= 'dot')
        #SVG(save)
