from setuptools import setup

setup(name='python_org_utils',
      version='0.0',
      description='package with objects to improve your organization',
      url='https://gitlab.com/R_addict/python_org_utils',
      author='Benoît Bergk Pinto',
      author_email='bergk_pinto@hotmail.com',
      license='MIT',
      packages=['python_org_utils'],
      scripts=['bin/pydotFlowChart_builder'],
      install_requires=[
          'pydot','numpy',
      ],
      zip_safe=False)
